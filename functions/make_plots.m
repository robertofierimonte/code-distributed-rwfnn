% MAKE_PLOTS - Plot information for the current simulation

%% General properties for the figures
width = 3.45;                   % Width in inches
height = 2.6;                   % Height in inches
font_size = 8;                  % Fontsize
font_size_leg = 6;              % Font size (legend)
font_name = 'TimesNewRoman';    % Font name
line_width = 1;                 % LineWidth
s = dataset.name;
%% Plot training time for different numbers of nodes

figure('name', 'Training time')
figshift;

N_distr = length(distributed_algorithms);
N_cent = length(centralized_algorithms);
N_local = length(local_algorithms);
tmp = mean(time, 1);
ds = std(time, 1);

leg = {};
for k = 1:N_cent
    plot([agents(1) - 5, agents(end) + 5], [tmp(k), tmp(k)],'--k', 'LineWidth', line_width);
    leg{end + 1} = centralized_algorithms{k}.name;
    hold on;
end
a = N_cent + 1;
for k = 1:N_distr
    errorbar(agents + 1, tmp(a:a+length(agents)-1), ds(a:a+length(agents)-1), 'r', 'LineWidth', line_width);
    leg{end + 1} = distributed_algorithms{k}.name;
    hold on;
    a = a + length(agents);
end

for k = 1:N_local
    errorbar(agents, tmp(a:a+length(agents) - 1), ds(a:a+length(agents)-1), 'b', 'LineWidth', line_width);
    leg{end + 1} = local_algorithms{k}.name;
    hold on;
    a = a + length(agents);
end

box on;
grid on;

xlabel('Number of agents', 'FontSize', font_size, 'FontName', font_name);
ylabel('Training time [s]', 'FontSize', font_size, 'FontName', font_name);

%ylim([0 110]);
h_legend = legend(leg, 'Location', 'NorthEast');


singlecolumn_format;

%export_fig(strcat(s,'_Training_time'),'-pdf', '-eps', '-painters', '-transparent');

%% Plot classification error for different numbers of nodes

figure('name', 'Test Error')
figshift;

N_distr = length(distributed_algorithms);
N_cent = length(centralized_algorithms);
tmp = mean(errors(:, :, 1), 1);
ds = std(errors(:, :, 1), 1);


leg = {};
for k = 1:N_cent
    plot([agents(1) - 5, agents(end) + 5], [tmp(k), tmp(k)], '--k', 'LineWidth', line_width);
    leg{end + 1} = centralized_algorithms{k}.name;
    hold on;
end
a = N_cent + 1;
for k = 1:N_distr
    errorbar(agents + 1, tmp(a:a+length(agents)-1), ds(a:a+length(agents)-1),'r', 'LineWidth', line_width);
    leg{end + 1} = distributed_algorithms{k}.name;
    hold on;
    a = a + length(agents);
end

for k = 1:N_distr
    errorbar(agents, tmp(a:a+length(agents)-1), ds(a:a+length(agents)-1),'b','LineWidth', line_width);
    leg{end + 1} = local_algorithms{k}.name;
    hold on;
    a = a + length(agents);
end

box on;
grid on;

xlabel('Number of agents', 'FontSize', font_size, 'FontName', font_name);
ylabel('Normalized Root-Mean-Square Error (NRMSE)', 'FontSize', font_size, 'FontName', font_name);

%ylim([0.05, 0.4]);
h_legend = legend(leg, 'Location', 'NorthEast');

singlecolumn_format;
%export_fig(strcat(s, '_Test_error'),'-pdf', '-eps', '-painters', '-transparent');