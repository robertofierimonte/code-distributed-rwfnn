function random_fis = random_fis(Xtr, Ytr, nMemberships, membershipType)
%RANDOM_FIS Returns a Sugeno Type-1 FIS where the parameters of the
%membership functions are randomly generated.

    [~, d] = size(Xtr);
    [~, o] = size(Ytr);  %In case of multiple outputs regression
    
    if length(nMemberships) == 1
        nMemberships = repmat(nMemberships, d, 1);
    end
    
    random_fis = newfis('random_weigths_fis', 'sugeno');
    
    comb = 1:nMemberships(1);
    for ii = 1:d
    
        random_fis = addvar(random_fis, 'input', strcat('x', num2str(ii)), [-1, 1]);
        
        for jj = 1:nMemberships(ii)
            %This section needs to be hard-coded, because the number and
            %the range of the parameters depends on the type of the
            %membership function
            if strcmp(membershipType, 'gaussmf')
                mean = min(Xtr(:, ii)) + (max(Xtr(:, ii)) - min(Xtr(:, ii)))*rand;            %the range for the mean should be inside the range for the variable
                ds = 2*rand;                    %the standard deviation must be positive 
                params = [mean, ds];
            elseif strcmp(membershipType, 'trimf')
                %define parameters for triangular function (not used)
            else
                %do something else (not used)
            end
            random_fis = addmf(random_fis,'input', ii, '', membershipType, params);
        end
        
        if (ii >= 2)
            comb = combvec(comb, 1:nMemberships(ii));
        end
    end
    
    random_fis = addvar(random_fis, 'output', 'y', [min(Ytr), max(Ytr)]);
        
    m = prod(nMemberships);
    for r = 1:m
        random_fis = addrule(random_fis, [comb(:, r)' r 1 1]);
        random_fis = addmf(random_fis, 'output', 1, strcat('outmf', r), 'linear', zeros(1, d + 1));
    end

end

