Distributed Learning of Random Weights Fuzzy Neural Networks

Description
-------
This library implements a decentralized learning algorithm for Random Weights 
Fuzzy Neural Networks (RWFNN)[1], when training data is distributed through a 
network of interconnected computing agents. The code is aimed at comparing the 
Alternating Direction Method of Multipliers [2] based RWFNN (ADMM-RWFNN) with 
three other models: ANFIS [3], Centralized-RWFNN and Local-RWFNN.

The paper describing the code is currently under review at the World Congress on
Computational Intelligence (WCCI 2016).
If you are interested in a pre-print copy, please contact me using the
information provided at the bottom of this file.


Usage 
-------
To launch a simulation, simply use the script 'test_script.m'. All the
configuration parameters (dataset, number of simulations, number of folds, 
nodes in the network and connectivity) are specified in the script 'params_selection'.
Currently, 4 algorithms are compared:

   * Alternating Direction Method of Multipliers Random Weights Fuzzy Neural Networks.
   * Adaptive Neuro-Fuzzy Inference System (ANFIS).
   * Centralized Random Weights Fuzzy Neural Networks.
   * Local Random Weights Fuzzy Neural Networks.




Licensing
---------
The code is distributed under BSD-2 license. Please see the file called LICENSE.

The code uses several utility functions from MATLAB Central. Copyright
information and licenses can be found in the 'functions' folder.

Network topology in folder 'network' and dataset splitting in folder 'Partition' are
adapted from the Lynx MATLAB toolbox:
https://github.com/ispamm/Lynx-Toolbox.


Contacts
--------

   o If you have any request, bug report, or inquiry, you can contact
     the author at roberto [dot] fierimonte [at] gmail [dot] com.


References
--------
[1] Y.-L. He, X.-Z. Wang, and J. Z. Huang, “Fuzzy nonlinear regression analysis using
a random weight network,” Information Sciences, 2016, in press.

[2] S. Boyd, N. Parikh, E. Chu, B. Peleato, and J. Eckstein, “Distributed optimization
and statistical learning via the alternating direction method of multipliers,” 
Foundations and Trends⃝R in Machine Learning, vol. 3, no. 1, pp. 1–122, 2011.

[3] J.-S. R. Jang, “ANFIS: adaptive-network-based fuzzy inference system,” IEEE 
Transactions on Systems, Man and Cybernetics, vol. 23, no. 3, pp. 665–685, 1993.