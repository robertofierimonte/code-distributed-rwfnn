classdef DistributedFNN < FNN & handle
    
    %A Distributed FNN learning model.
    
    properties
    end
    
    methods
        function obj = DistributedFNN(name, lambda, solver)
            obj = obj@FNN(name, lambda, solver);
        end
        
        function obj = setFis(obj, fis)
            obj = setFis@FNN(obj, fis);
        end
        
        function [obj, trainInfo] = train(obj, net, trainData)
            
            N_nodes = net.N;
            beta0 = zeros(size(obj.fis.rule, 2)*(size(trainData.X, 2) + 1), N_nodes); 
            
            Hinv = cell(N_nodes, 1);        
            
            for ii = 1:N_nodes
                
                trainLocal = getLocalPart(trainData, ii);
                Xtr = trainLocal.X;
                Ytr = trainLocal.Y;
                
                Hinv{ii} = obj.computeHiddenMatrix(Xtr);
                [~, p] = size(Hinv{ii});
                
                beta0(:, ii) = (Hinv{ii}' * Hinv{ii} + obj.params.lambda)\ (Hinv{ii}' * Ytr);
            end
            
            [obj, trainInfo] = obj.solver.solve(obj, net, trainData, beta0);
        end
        
    end
end