classdef LeastSquaresSolverForFNN
    
    %A least squares algorithm for Fuzzy Neural Networks.
    %The antecedent parameters are fixed, and the consequent parameters are
    %estimated using the LS algorithm.
    
    properties
    end
    
    methods
        
        function [fnn, info] = solve(~, fnn, trainData)
            
            info = struct();
            
            Xtr = trainData.X;
            [~, d] = size(trainData.X);
            
            
            Ytr = trainData.Y;
            
            H = fnn.computeHiddenMatrix(Xtr);
            [~, p] = size(H);
            
            boptimal = (H'*H + fnn.params.lambda*eye(p)) \ (H'*Ytr);
            
            for i = 1:size(fnn.fis.output.mf, 2)
                fnn.fis.output.mf(i).params = boptimal((i - 1)*(d + 1) + 1: i*(d + 1))';
            end
        end
    end
end