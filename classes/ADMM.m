classdef ADMM
    
    properties
        consensus_max_steps;
        consensus_thres;
        admm_max_steps;
        admm_rho;
        admm_reltol;
        admm_abstol;
    end
    
    methods
        
        function obj = ADMM(varargin)
            p = inputParser();
            p.addParameter('consensus_max_steps', 300);
            p.addParameter('consensus_thres', 0.001);
            p.addParameter('admm_max_steps', 300);
            p.addParameter('admm_rho', 1);
            p.addParameter('admm_reltol', 0.001);
            p.addParameter('admm_abstol', 0.001);
            p.parse(varargin{:});
            obj.consensus_max_steps = p.Results.consensus_max_steps;
            obj.consensus_thres = p.Results.consensus_thres;
            obj.admm_max_steps = p.Results.admm_max_steps;
            obj.admm_rho = p.Results.admm_rho;
            obj.admm_reltol = p.Results.admm_reltol;
            obj.admm_abstol = p.Results.admm_abstol;
        end
        
        function [alg, trainInfo] = solve(obj, alg, net, trainData, ~)
            
            N_nodes = net.N;
            [~, d] = size(trainData.X);
            beta0 = zeros(size(alg.fis.rule, 2)*(d + 1), N_nodes);
            N_params = size(beta0, 1);
            
            error = zeros(obj.admm_max_steps, 1);
            consensus_steps = zeros(obj.admm_max_steps, 1);
            
             % Global term
            z = zeros(N_params, 1);
            
            % Lagrange multipliers
            t = zeros(N_params, N_nodes);
            
            % Parameters
            rho = obj.admm_rho;
            steps = obj.admm_max_steps;
            
            % Precompute the inverse matrices
            Hinv = cell(N_nodes, 1);
            HY = cell(N_nodes, 1);
            
            for ii = 1:N_nodes
                
                d_local = getLocalPart(trainData, ii);
                Xtr = d_local.X;
                
                Hinv{ii} = alg.computeHiddenMatrix(Xtr);
                HY{ii} = Hinv{ii}'*d_local.Y;
                
                if(size(Xtr, 1) > length(beta0))
                    Hinv{ii} = inv(eye(length(beta0))*rho + Hinv{ii}' * Hinv{ii});
                else
                    Hinv{ii} = (1/rho)*(eye(length(beta0)) - Hinv{ii}'*inv(rho*eye(size(Xtr, 1)) + Hinv{ii}*Hinv{ii}')*Hinv{ii});
                end
                
                
            end
            
            beta = beta0;
            
            for ii = 1:steps
                
                for jj = 1:N_nodes
                    
                    % Compute current weights
                    beta(:, jj) = Hinv{jj}*(HY{jj} + rho*z - t(:, jj));
                    
                end
                
                % Run consensus
                consensus = DistributedAverageConsensus('consensus_max_steps', obj.consensus_max_steps, 'consensus_thres', obj.consensus_thres);
                [beta_avg, ~] = consensus.solve(obj, net, trainData, beta);
                [t_avg, ~] = consensus.solve(obj, net, trainData, t);
                
                
                % Store the old z and update it
                zold = z;
                z = (rho*beta_avg + t_avg)/(alg.params.lambda/N_nodes + rho);
                
                % Compute the update for the Lagrangian multipliers
                for jj = 1:N_nodes
                    t(:, jj) = t(:, jj) + rho*(beta(:, jj) - z);
                end
                
                % Check stopping criterion
                s = - rho*(z - zold);
                t_norm = zeros(N_nodes, 1);
                
                primal_criterion = zeros(N_nodes, 1);
                
                for kk = 1:N_nodes
                    r = beta(:, kk) - z;
                    if norm(r) < sqrt(N_nodes)*obj.admm_abstol + obj.admm_reltol*max(norm(beta(:,kk), 2), norm(z, 2)) 
                        primal_criterion(kk) = 1;
                    end
                    
                    t_norm(kk) = norm(t(:, kk), 2);
                end
                
                if norm(s) < sqrt(N_nodes)*obj.admm_abstol + obj.admm_reltol*max(t_norm) && max(primal_criterion) == 1
                    break;
                end
            end
            
            boptimal = beta(:, 1);
            
            for i = 1:size(alg.fis.output.mf, 2)
                alg.fis.output.mf(i).params = boptimal((i - 1)*(d + 1) + 1: i*(d + 1))';
            end    
            
            trainInfo.error = error;
            trainInfo.consensus_steps = consensus_steps;
            trainInfo.steps = ii;
        end
    end
    
end

