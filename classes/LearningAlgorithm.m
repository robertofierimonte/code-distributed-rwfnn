classdef LearningAlgorithm < handle
    % LEARNINGALGORITHM - Simple abstract class for representing a generic
    % learning algorithm.
    properties
        name; % Name of the algorithm
        params; % Parameters of the algorithm
    end
    
    methods
        function obj = LearningAlgorithm(name)
            % Constructor for LearningAlgorithm object
            obj.name = name;
            obj.params = struct();
        end
    end
    
    methods (Abstract)
        % Train the algorithm using a training set
        obj = train(obj, Dataset);
        
        % Test the algorithm using a test set
        obj = test(obj, Dataset);
    end
end

