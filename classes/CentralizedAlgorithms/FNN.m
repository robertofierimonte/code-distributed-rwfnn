classdef FNN < LearningAlgorithm & handle
    
    %A Fuzzy Neural Network (FNN) learning model.
    
    properties
        fis;        %Structure containing the FIS
        solver;     %Algorithm used to train the network
    end
    
    methods
        function obj = FNN(name, lambda, solver)
            obj = obj@LearningAlgorithm(name);
            obj.params.lambda = lambda;
            obj.solver = solver;
            obj.fis = [];
        end
        
        function [obj, trainInfo] = train(obj, trainData)
            
            %Extracts the antecedent parameters from the fis(currently unused)
            alpha0 = getMembershipWeights(obj);
            
            %Trains the Model using an appropriate solver
            [obj, trainInfo] = obj.solver.solve(obj, trainData);
            
        end
        
        function obj = setFis(obj, fisStructure)
            obj.fis = fisStructure;
        end
        
        function [Yhat, RMSE] = test(obj, testData)   
            
            Xtst = testData.X;
            Ytst = testData.Y;
            
            if isa(obj.solver, 'HybridTrainingAlgorithm')
                Yhat = evalfis(Xtst, obj.fis);
            else
                H = obj.computeHiddenMatrix(Xtst);

                beta = [];
                for ii = 1:size(obj.fis.output.mf, 2)
                    beta = [beta; obj.fis.output.mf(ii).params'];
                end

                Yhat = H * beta;
            end
            
            RMSE = sqrt(norm(Ytst - Yhat)^2/length(Ytst));
        end

        function alpha = getMembershipWeights(obj)
            
            alpha = [];
            for i = obj.fis.input
                for j = i.mf
                    alpha = [alpha, j.params]; %Extract the antecedent parameters from the fis
                end
            end
            
        end
        
        function H = computeHiddenMatrix(obj, X)
            
            [N, d] = size(X);
            nRules = size(obj.fis.rule, 2);
            
            a = zeros(nRules, N, d);
            
            for ii = 1:nRules
                for jj = 1:d
                    mf = obj.fis.input(jj).mf(obj.fis.rule(ii).antecedent(jj));
                    a(ii, :, jj) = evalmf(X(:, jj), mf.params, mf.type);
                end
            end
            
            w = prod(a, 3); %Computes the unnormalized firing strengths
            w_hat = w./(repmat(sum(w, 1), nRules, 1)); %Normalizes the firing strengths
            w_hat(find(isnan(w_hat))) = 1/nRules; 
            H = [];
            for c = 1:size(w_hat, 1)
                H = [H repmat(w_hat(c, :)', 1, d+1).*[ones(N, 1) X]]; %Computes the hidden matrix
            end
        end
    end
    
end

